
function showTemplatebyDrawer(args) {
  $("#x-drawer").remove();
  // 拼接URL
  let url = args["templateUrl"];
  for (var i in args["params"]) {
    let value = args["params"][i] !== undefined ? args["params"][i] : '';
    url += `&${i}=${value}`
  }
  // urlencode
  url = FR.cjkEncode(url);
  // 定义弹窗所用的iframe元素
  var $iframe = $("<iframe  width='100%' height='100%' scrolling='no' frameborder='0'>");
  // 赋予iframe打开的地址为上面定义的报表连接地址
  $iframe.attr("src", url);
  if (args.hasOwnProperty("titleHtml")) {
    var titleDiv = $(args["titleHtml"]);
  } else {
    var titleDiv = $("<p>" + args["title"] + "</p>");
  }
  titleDiv.css({ "float": "left", "margin-left": "10px", "margin-top": "10px" });
  if (args.hasOwnProperty("titleStyle")) {
    titleDiv.css(args["titleStyle"]);
  } else {
    titleDiv.css({ "font-size": "16px", "font-weight": 800 });
  }
  var closeBtn = $('<?xml version="1.0" encoding="UTF-8"?><svg width="20" height="20" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 8L40 40" stroke="#000000" stroke-width="3" stroke-linecap="butt" stroke-linejoin="bevel"/><path d="M8 40L40 8" stroke="#000000" stroke-width="3" stroke-linecap="butt" stroke-linejoin="bevel"/></svg>').css({ "float": "right", "margin-right": "20px", "margin-top": "10px", "cursor": "pointer" });
  closeBtn.click(function () {
    $("#x-drawer").hide();
    $("#x-drawer").remove();
  }).hover(function () {
    $(this).find("path").attr("stroke", "#4e8ac9");
  }, function () {
    $(this).find("path").attr("stroke", "#000");
  })
  var topDiv = $("<div></div>").css({ "width": "100%", "height": "60px" });
  var bottomDiv = $("<div></div>").css({ "width": "100%", "height": "calc(100% - 80px)" });
  var outerDiv = $("<div id='x-drawer' name='x-drawer'></div>");
  outerDiv.css({
    "box-shadow": "0px 0px 80px rgba(0, 0, 0, 0.4)",
    "background": "#fff",
    "padding": "10px",
    "position": "fixed",
    "z- index": "1000",
    "transition": "all 1s ease -in -out",
  });

  switch (args["positon"]) {
    case ("right"):
      outerDiv.css({
        "top": "0",
        "right": "0px",
        "width": args["size"],
        "height": "calc(100% - 20px)",
      })
      break;
    case ("left"):
      outerDiv.css({
        "top": "0",
        "left": "0px",
        "width": args["size"],
        "height": "calc(100% - 20px)",
      })
      break;
    case ("top"):
      outerDiv.css({
        "top": "0",
        "left": "0px",
        "right": "0px",
        "width": "calc(100% - 20px)",
        "height": args["size"],
      })
      break;
    case ("bottom"):
      outerDiv.css({
        "bottom": "0",
        "width": "calc(100% - 20px)",
        "height": args["size"],
      })
      break;
    default:
      outerDiv.css({
        "top": "0",
        "right": "0px",
        "width": args["size"],
        "height": "calc(100% - 20px)",
      })
      break;
  }
  titleDiv.appendTo(topDiv);
  closeBtn.appendTo(topDiv);
  topDiv.appendTo(outerDiv);
  $iframe.appendTo(bottomDiv);
  bottomDiv.appendTo(outerDiv);
  outerDiv.appendTo("body");
}