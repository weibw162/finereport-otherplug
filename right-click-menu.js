// 点击任意地方隐藏右键菜单
$(document).click(function () {
  $('#right-click-menu').remove();
})

function RCM(el, menulist, boxStyle, itemHoverColor) {

  el.on('contextmenu', function (e) {


    let domdiv = $('<div id="right-click-menu"></div>')
    if (boxStyle) {
      domdiv.css(boxStyle);
    } else {
      domdiv.css({ "width": "200px", "background": "#f3f3f3", "border": "1px solid #D3D3D3", "padding": "10px 10px" });
    }
    domdiv.css({ "display": "none", "z-index": 9999, "position": "absolute" })
    let domul = $('<ul></ul>').css({ "list-style-type": "none", "padding": 0, "margin": 0 });

    menulist.forEach(e => {
      if (e.hasOwnProperty("html")) {
        var dom = $("<li>" + e["html"] + "</li>");
      } else {
        var dom = $("<li>" + e["name"] + "</li>");
      }

      switch (e["type"]) {
        case ("dialog"):
          dom.on("click", function () {
            showTemplatebyDialog(e["args"]);
          });
          break;
        case ("fsnewtab"):
          dom.on("click", function () {
            showTemplatebyFSNewtab(e["args"]);
          });
          break;
        case ("fsselftab"):
          dom.on("click", function () {
            showTemplatebyFSSelftab(e["args"]);
          });
          break;
        case ("newwindow"):
          dom.on("click", function () {
            showTemplatebyNewWindow(e["args"]);
          });
          break;
        case ("selfwindow"):
          dom.on("click", function () {
            showTemplatebySelfWindow(e["args"]);
          });
          break;
        case ("drawer"):
          dom.on("click", function () {
            showTemplatebyDrawer(e["args"]);
          });
          break;
        case ("func"):
          dom.on("click", function () {
            doFunc(e["args"]);
          });
          break;
        default:
          break;
      }
      if (e.hasOwnProperty("style")) {
        dom.css(e["style"]);
      } else {
        dom.css({ "white-space": "nowrap", "cursor": "pointer", "font-size": "12px", "height": "24px", "line-height": "24px" });
      }
      dom.css({ "padding": "3px 0" });
      var dombackground = dom.css("background");
      dom.hover(function () { $(this).css({ "background": itemHoverColor || "#e2e2e2" }) }, function () { $(this).css({ "background": dombackground }) }).appendTo(domul);
    });
    domdiv.mouseleave(function () { $(this).remove() });
    domul.appendTo(domdiv);
    domdiv.appendTo("body");

    const PADDING_RIGHT = 6  // 右边留点空位，防止直接贴边了，不好看
    const PADDING_BOTTOM = 6  // 底部也留点空位
    const vw = document.documentElement.clientWidth
    const vh = document.documentElement.clientHeight

    x = e.pageX;
    y = e.pageY;
    w = 200;
    h = 30 * menulist.length;
    if (x + w > vw - PADDING_RIGHT) x -= w
    if (y + h > vh - PADDING_BOTTOM) y -= h



    $('#right-click-menu').show().css({
      'overflow': 'auto',
      'top': y + 'px',
      'left': x + 'px'
    });
    e.preventDefault();
  });


}

// 使用弹窗显示模板
function showTemplatebyDialog(args) {
  // 拼接URL
  let url = args["templateUrl"];
  for (var i in args["params"]) {
    let value = args["params"][i] !== undefined ? args["params"][i] : '';
    url += `&${i}=${value}`
  }
  // urlencode
  url = FR.cjkEncode(url);
  // 定义弹窗所用的iframe元素
  var $iframe = $("<iframe id='x-dialog' name='x-dialog' width='100%' height='100%' scrolling='no' frameborder='0'>");
  // 赋予iframe打开的地址为上面定义的报表连接地址
  $iframe.attr("src", url);
  // 显示弹窗
  FR.showDialog(args["title"], args["width"], args["height"], $iframe)
}

// 平台内新标签显示模板
function showTemplatebyFSNewtab(args) {
  FR.doHyperlinkByGet4Reportlet({

    "url": args["templateUrl"],
    "para": args["params"],
    "target": "_blank_fs",
    "feature": { "width": 600, "height": 400, "isCenter": true, "title": "" },
    "title": args["title"]
  })
}

// 平台内当前标签显示模板
function showTemplatebyFSSelftab(args) {
  FR.doHyperlinkByGet4Reportlet({
    "url": args["templateUrl"],
    "para": args["params"],
    "target": "_self_fs",
    "feature": { "width": 600, "height": 400, "isCenter": true, "title": "" },
    "title": args["title"]
  })
}

// 当前窗口显示模板
function showTemplatebyNewWindow(args) {
  FR.doHyperlinkByGet4Reportlet({

    "url": args["templateUrl"],
    "para": args["params"],
    "target": "_blank",
    "feature": { "width": 600, "height": 400, "isCenter": true, "title": "" },
    "title": args["title"]
  })
}

// 当前窗口显示模板
function showTemplatebySelfWindow(args) {
  FR.doHyperlinkByGet4Reportlet({

    "url": args["templateUrl"],
    "para": args["params"],
    "target": "_self",
    "feature": { "width": 600, "height": 400, "isCenter": true, "title": "" },
    "title": args["title"]
  })
}

// 执行方法
function doFunc(args) {
  args["func"]();
}

// 抽屉展示
function showTemplatebyDrawer(args) {
  $("#x-drawer").remove();
  // 拼接URL
  let url = args["templateUrl"];
  for (var i in args["params"]) {
    let value = args["params"][i] !== undefined ? args["params"][i] : '';
    url += `&${i}=${value}`
  }
  // urlencode
  url = FR.cjkEncode(url);
  // 定义弹窗所用的iframe元素
  var $iframe = $("<iframe  width='100%' height='100%' scrolling='no' frameborder='0'>");
  // 赋予iframe打开的地址为上面定义的报表连接地址
  $iframe.attr("src", url);
  if (args.hasOwnProperty("titleHtml")) {
    var titleDiv = $(args["titleHtml"]);
  } else {
    var titleDiv = $("<p>" + args["title"] + "</p>");
  }
  titleDiv.css({ "float": "left", "margin-left": "10px", "margin-top": "10px" });
  if (args.hasOwnProperty("titleStyle")) {
    titleDiv.css(args["titleStyle"]);
  } else {
    titleDiv.css({ "font-size": "16px", "font-weight": 800 });
  }
  var closeBtn = $('<?xml version="1.0" encoding="UTF-8"?><svg width="20" height="20" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 8L40 40" stroke="#000000" stroke-width="3" stroke-linecap="butt" stroke-linejoin="bevel"/><path d="M8 40L40 8" stroke="#000000" stroke-width="3" stroke-linecap="butt" stroke-linejoin="bevel"/></svg>').css({ "float": "right", "margin-right": "20px", "margin-top": "10px", "cursor": "pointer" });
  closeBtn.click(function () {
    $("#x-drawer").hide();
    $("#x-drawer").remove();
  }).hover(function () {
    $(this).find("path").attr("stroke", "#4e8ac9");
  }, function () {
    $(this).find("path").attr("stroke", "#000");
  })
  var topDiv = $("<div></div>").css({ "width": "100%", "height": "60px" });
  var bottomDiv = $("<div></div>").css({ "width": "100%", "height": "calc(100% - 80px)" });
  var outerDiv = $("<div id='x-drawer' name='x-drawer'></div>");
  outerDiv.css({
    "box-shadow": "0px 0px 80px rgba(0, 0, 0, 0.4)",
    "background": "#fff",
    "padding": "10px",
    "position": "fixed",
    "z- index": "1000",
    "transition": "all 1s ease -in -out",
  });

  switch (args["positon"]) {
    case ("right"):
      outerDiv.css({
        "top": "0",
        "right": "0px",
        "width": args["size"],
        "height": "calc(100% - 20px)",
      })
      break;
    case ("left"):
      outerDiv.css({
        "top": "0",
        "left": "0px",
        "width": args["size"],
        "height": "calc(100% - 20px)",
      })
      break;
    case ("top"):
      outerDiv.css({
        "top": "0",
        "left": "0px",
        "right": "0px",
        "width": "calc(100% - 20px)",
        "height": args["size"],
      })
      break;
    case ("bottom"):
      outerDiv.css({
        "bottom": "0",
        "width": "calc(100% - 20px)",
        "height": args["size"],
      })
      break;
    default:
      outerDiv.css({
        "top": "0",
        "right": "0px",
        "width": args["size"],
        "height": "calc(100% - 20px)",
      })
      break;
  }
  titleDiv.appendTo(topDiv);
  closeBtn.appendTo(topDiv);
  topDiv.appendTo(outerDiv);
  $iframe.appendTo(bottomDiv);
  bottomDiv.appendTo(outerDiv);
  outerDiv.appendTo("body");
}