function EnlargePic(args) {
  var el = args["el"];
  var type = args["type"];
  var args = args["args"];
  if (el && type) {
    switch (type) {
      case ("followmouse"):
        FollowMouse(el, args);
        break;
      case ("screencenter"):
        ScreenCenter(el, args);
        break;
      case ("click"):
        Click(el, args);
        break;
    }
  }
}

function FollowMouse(el, args) {
  var x = 10;
  var y = 20;
  var width = args ? args["width"] || 200 : 200;
  var height = args ? args["height"] || 150 : 150;

  el.each(function () {
    $(this).mouseover(function (e) {
      var b = $(this).find("img").size() > 0 ? $(this).find("img") : $(this);
      var imgUrl = b.attr("src") || b.text();
      var imgBox = "<div id='imgBox'><img src='" + imgUrl + "' width='" + width + "' height='" + height + "'></img><div>";
      $("body").append(imgBox);
      $("#imgBox").css({
        position: "absolute",
        'top': (window.innerHeight - e.pageY - y - height < 20 ? (window.innerHeight - height - 20) : (e.pageY + y)) + "px",
        "left": (window.innerWidth - e.pageX - x - width < 20 ? (window.innerWidth - width - 20) : (e.pageX + x)) + "px"
      }).show("fast");
    }).mouseout(function () {
      $("#imgBox").remove();
    }).mousemove(function (e) {
      $("#imgBox").css({
        position: "absolute",
        'top': (window.innerHeight - e.pageY - y - height < 20 ? (window.innerHeight - height - 20) : (e.pageY + y)) + "px",
        "left": (window.innerWidth - e.pageX - x - width < 20 ? (window.innerWidth - width - 20) : (e.pageX + x)) + "px"
      });
    });
  })



}

function ScreenCenter(el, args) {
  var width = args ? args["width"] || 200 : 200;
  var height = args ? args["height"] || 150 : 150;
  el.each(function () {
    $(this).mouseover(function (e) {
      $("#imgBox").remove();
      var b = $(this).find("img").size() > 0 ? $(this).find("img") : $(this);
      var imgUrl = b.attr("src") || b.text();
      var imgBox = "<div id='imgBox'><img src='" + imgUrl + "' width='" + width + "' height='" + height + "'></img><div>";
      $("body").append(imgBox);
      $("#imgBox").css({
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%,-50%)"
      }).show("fast");
    }).mouseout(function () {
      $("#imgBox").remove();
    })
  })
}

function Click(el, args) {
  var width = args ? args["width"] || 200 : 200;
  var height = args ? args["height"] || 150 : 150;
  el.each(function () {
    $(this).click(function (e) {
      $("#imgBox").remove();
      var b = $(this).find("img").size() > 0 ? $(this).find("img") : $(this);
      var imgUrl = b.attr("src") || b.text();
      var imgBox = "<div id='imgBox'><img src='" + imgUrl + "' width='" + width + "' height='" + height + "'></img><div id='closeBox' title='关闭'>+</div><div>";
      $("body").append(imgBox);

      $("#imgBox").css({
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%,-50%)"

      }).show("fast");
      $("#closeBox").css({
        position: "absolute",
        width: "36px",
        height: "36px",
        "font-size": "36px",
        "font-weight": "600",
        "color": "red",
        "text-alight": "middle",
        top: "-36px",
        right: "-36px",
        transform: "rotate(45deg)",
        cursor: "pointer"
      }).click(function () {
        $("#imgBox").remove();
      })
    })
  })
}

