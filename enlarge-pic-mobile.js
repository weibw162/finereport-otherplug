function EnlargePicMobile(args) {

  var el = args["el"];
  var isMask = args.hasOwnProperty("isMask") ? args["isMask"] : true;
  var width = ((args["width"] || '90') * window.screen.width) / 100 + "px";
  var height = args["height"] ? (((args["height"] || '35') * window.screen.height) / 100 + "px") : args["height"];
  var closeColor = args["closeColor"] || 'red';
  var imgOpacity = args["imgOpacity"] || 1;
  var maskColor = args["maskColor"] || 'gray';
  var maskOpacity = args["maskOpacity"] || 0.65;
  el.each(function () {
    $(this).click(function (e) {
      $("#imgBox").remove();
      var b = $(this).find("img").size() > 0 ? $(this).find("img") : $(this);
      var imgUrl = b.attr("src") || b.text();
      var imgBox = "<div id='imgBox'><img src='" + imgUrl + "' width='" + width + (height ? ("' height='" + height) : "") + "'></img><div id='closeBox' title='关闭'>+</div><div>";

      $("body").append(imgBox);
      $("#imgBox").css({
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%,-50%)",
        transition: 'opacity 1s ease',
        opacity: 0,
        'z-index': 999

      }).animate({ opacity: imgOpacity }, 20);
      $("#closeBox").css({
        "font-size": "64px",
        "font-weight": "800",
        "color": closeColor,
        "text-align": "center",
        top: '20px',
        transform: "rotate(45deg)",
        cursor: "pointer"
      }).click(function () {
        $("#imgBox").remove();
        $("#maskDiv").remove();
      });
      if (isMask) {
        var maskDiv = '<div id="maskDiv"></div>';
        $("body").append(maskDiv);
        $("#maskDiv").css({
          background: maskColor,
          position: 'fixed',
          left: '0px',
          top: '0px',
          width: '100%',
          height: '100%',
          'z-index': 998,
          opacity: maskOpacity

        }).show("slow");
      }
    })
  })

}


